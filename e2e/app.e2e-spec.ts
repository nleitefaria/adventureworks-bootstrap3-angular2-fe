import { AdventureworksBootstrap4Angular4FePage } from './app.po';

describe('adventureworks-bootstrap4-angular4-fe App', () => {
  let page: AdventureworksBootstrap4Angular4FePage;

  beforeEach(() => {
    page = new AdventureworksBootstrap4Angular4FePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
