import { Component, OnInit } from '@angular/core';
import { CustomerService } from './customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  providers: [CustomerService]
})

export class CustomerComponent implements OnInit
{
  	tr: any;
  	customers: any;
  
    constructor(private httpService : CustomerService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  
  	onclick()
  	{  
 	}
    
	paginate(event)
 	{   
    	this.getDataForPage(event.page); 
	}
      
    init()
    {  
    	this.httpService.getdata("0").subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.customers = response.content;				
					this.tr = response.totalPages;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.customers = response.content;				
					this.tr = response.totalPages;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }
}