import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { AppRoutingModule } from './app-routing.module';

import {ButtonModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';

import { AppComponent } from './app.component';
import { ProductdescriptionComponent } from './productdescription/productdescription.component';
import { ProductmodelComponent } from './productmodel/productmodel.component';
import { AddressComponent } from './address/address.component';
import { CustomerComponent } from './customer/customer.component';
import { ProductComponent } from './product/product.component';
import { ProductcategoryComponent } from './productcategory/productcategory.component';
import { SalesorderdetailComponent } from './salesorderdetail/salesorderdetail.component';
import { SalesorderheaderComponent } from './salesorderheader/salesorderheader.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductdescriptionComponent,
    ProductmodelComponent,
    AddressComponent,
    CustomerComponent,
    ProductComponent,
    ProductcategoryComponent,
    SalesorderdetailComponent,
    SalesorderheaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgxPaginationModule,
    ButtonModule,
    PaginatorModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
