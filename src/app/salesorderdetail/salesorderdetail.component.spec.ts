import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesorderdetailComponent } from './salesorderdetail.component';

describe('SalesorderdetailComponent', () => {
  let component: SalesorderdetailComponent;
  let fixture: ComponentFixture<SalesorderdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesorderdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesorderdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
