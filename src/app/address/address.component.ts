import { Component, OnInit} from '@angular/core';
import { AddressService } from './address.service';


@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css'],
  providers: [AddressService]
})

export class AddressComponent  
{
	tr: any;
  	addresses: any;
  
    constructor(private httpService : AddressService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  
  	onclick()
  	{  
 	}
    
	paginate(event)
 	{   
    	this.getDataForPage(event.page); 
	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.addresses = response.content;				
					this.tr = response.totalPages;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.addresses = response.content;				
					this.tr = response.totalPages;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }
}