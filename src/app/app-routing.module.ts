import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ProductdescriptionComponent } from './productdescription/productdescription.component';
import { ProductmodelComponent } from './productmodel/productmodel.component';
import { AddressComponent } from './address/address.component';
import { CustomerComponent } from './customer/customer.component';
import { ProductComponent } from './product/product.component';
import { ProductcategoryComponent } from './productcategory/productcategory.component';
import { SalesorderdetailComponent } from './salesorderdetail/salesorderdetail.component';
import { SalesorderheaderComponent } from './salesorderheader/salesorderheader.component';

const routes: Routes = [
  {
    path: '',
    children: []
  },
  { path: 'productdescription', component: ProductdescriptionComponent },
  { path: 'productmodel', component: ProductmodelComponent },
  { path: 'address', component: AddressComponent },
  { path: 'customer', component: CustomerComponent },
  { path: 'product', component: ProductComponent },
  { path: 'productcategory', component: ProductcategoryComponent },
  { path: 'salesorderdetail', component: SalesorderdetailComponent },
  { path: 'salesorderheader', component: SalesorderheaderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }