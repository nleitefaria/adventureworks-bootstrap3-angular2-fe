import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesorderheaderComponent } from './salesorderheader.component';

describe('SalesorderheaderComponent', () => {
  let component: SalesorderheaderComponent;
  let fixture: ComponentFixture<SalesorderheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesorderheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesorderheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
