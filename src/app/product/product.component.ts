import { Component, OnInit } from '@angular/core';
import { ProductService } from './product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ProductService]
})
export class ProductComponent implements OnInit 
{
	tr: any;
  	products: any;
  
    constructor(private httpService : ProductService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  
  	onclick()
  	{  
 	}
    
	paginate(event)
 	{   
    	this.getDataForPage(event.page); 
	}
      
    init()
    {  
    	this.httpService.getdata("0").subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.products = response.content;				
					this.tr = response.totalPages;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.products = response.content;				
					this.tr = response.totalPages;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }

  
}
