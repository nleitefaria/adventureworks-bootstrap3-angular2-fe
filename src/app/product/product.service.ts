import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductService 
{
  	private BASE_URL:string = 'http://localhost:8090/products/';
	
	constructor(
		private http:Http
	) { }
 
	public getdata(pageNum:string):any{
		return this.http.get('http://localhost:8090/products/' + pageNum + '/10')
			.map((response:Response) => response.json())
			.catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
	}

}
